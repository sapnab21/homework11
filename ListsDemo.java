import java.util.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListsDemo {
    public static void main(String[] args) {

        List<Integer> array1 = new ArrayList<>();
        array1.add(-2); array1.add(-3); array1.add(6); array1.add(3);
        System.out.println("Integers in array #1: " + array1);

        List<Integer> array2 = new ArrayList<>();
        array2.add(-2); array2.add(-4); array2.add(5); array2.add(-3);
        System.out.println("Integers in array #2: " + array2);

        List<Integer> array3 = new ArrayList<>();
        array3.addAll(array1); array3.addAll(array2);
        System.out.println("Integers in array #3: " + array3);


        Collections.sort(array3);
        System.out.print("Minimum integer in array #3: ");
        System.out.println(array3.get(0));
            }
        }



